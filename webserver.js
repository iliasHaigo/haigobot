const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
const logger = require("./src/log4js/log4jsConf");

module.exports = {
    initServer : function () {
    // Load client secrets from a local file.
    initServer();
    }
  };

function initServer() {

    logger.info("Web server initialization");
    
    router.get('/',function(req,res){
      res.sendFile(path.join(__dirname+'/index.html'));
    });
    
    router.get('/about',function(req,res){
      res.sendFile(path.join(__dirname+'/about.html'));
    });
    
    //add the router
    app.use(express.static(__dirname + '/View'));
    //Store all HTML files in view folder.
    app.use(express.static(__dirname + '/Script'));
    //Store all JS and CSS in Scripts folder.
    app.use(express.static(__dirname + '/dg'));
    app.use(express.static(__dirname + '/dist'));
    app.use(express.static(__dirname + '/node_modules'));
    
    
    app.use('/', router);
    app.listen(process.env.port || 8080);
    
    logger.info('Web server running at Port 8080');
}

