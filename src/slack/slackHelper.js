//requirements
var SlackBot = require("slackbots");

var userList = [];
var channelList = [];

module.exports = {
    generateUserList: function (bot) {
        return generateUserList(bot);
    },
    userListToString: function () {
        return userListToString();
    },
    getUsernameFromId: function (id) {
        return getUsernameFromId(id);
    },
    getUseridFromName: function (name) {
        return getUseridFromName(name);
    },
    generateChannelList: function (bot) {
        return generateChannelList(bot);
    },
    channelListToString: function () {
        return channelListToString();
    },
    getChannelnameFromId: function (id) {
        return getChannelnameFromId(id);
    },
    getChannelidFromName: function (name) {
        return getChannelidFromName(name);
    }
};

function generateUserList(bot) {
    userList = bot.getUsers()._value.members;
    console.log("user list size : " + userList.length);
}

function userListToString() {
    if (userList.length) {
        var userListString = "";
        userList.forEach(user => {
            userListString += "User id : [" + user.id + "]. User name : [" + user.name + "]. User real name : [" + user.real_name + "] \n";
        });

        return userListString;
    }

    return "No users in list";
}

function getUsernameFromId(id) {
    for (let index = 0; index < userList.length; index++) {
        const user = userList[index];
        if (user.id == id) {
            return user.name;
        }

    }
    return "User Not Found";
}

function getUseridFromName(name) {
    for (let index = 0; index < userList.length; index++) {
        const user = array[index];
        if (user.name == name) {
            return user.id;
        }

    }
    return "User Not Found";
}

function generateChannelList(bot) {
    channelList = bot.getChannels()._value.channels;
    console.log("channel list size : " + channelList.length);
}

function channelListToString() {
    if (channelList.length) {
        var channelListString = "";
        channelList.forEach(channel => {
            channelListString += "Channel id : [" + channel.id + "]. Channel name : [" + channel.name + "]. Is Channel archived ? : [" + channel.is_archived + "] \n";
        });

        return channelListString;
    }

    return "No channels in list";
}

function getChannelnameFromId(id) {
    for (let index = 0; index < channelList.length; index++) {
        const channel = channelList[index];
        if (channel.id == id) {
            return channel.name;
        }

    }
    return "Channel Not Found";
}

function getChannelidFromName(name) {
    for (let index = 0; index < channelList.length; index++) {
        const channel = array[index];
        if (channel.name == name) {
            return channel.id;
        }

    }
    return "Channel Not Found";
}