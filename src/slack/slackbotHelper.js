//requirements
var SlackBot = require("slackbots");
var slackHelper = require("../slack/slackHelper");
var dateFormat = require('dateformat');

//spreadsheets
var gssMB = require("../gsuite/gssMargeBrute");
var gssQA = require("../gsuite/gssQuestionsReponses");
var gssPS = require("../gsuite/gssPlanningStaffing");
var gssBills = require("../gsuite/gssBills");

//logger
const logger = require("../log4js/log4jsConf");

//global configuration
const botName = "Marvin";
const languageLocale = "fr-FR";
var devChannel = "haigobot";
var questionsChannel = "slackbot_question";

var bot;

module.exports = {
    initSlackbot: function () {
        initSlackbot();
    },
    postDevChannel: function (message) {
        postDevChannel(message);
    },
    postToChannel: function (message, channel) {
        postToChannel(message, channel);
    },
    postToUser: function (user, message) {
        bot.postMessageToUser(user, message);
    },
    botConnect: function () {
        bot.connect();
    },
    billsReminder: function() {
        handleBillsReminder();
    },
    planningReminder: function(notifyManager) {
        handlePlanningReminderCommand(notifyManager);
    }
};


function initSlackbot() {
    //initialisation du bot
    logger.info("Bot initialization");
    bot = new SlackBot({
        token: "xoxb-38878104645-552430105109-OCCCBqLAvrkmNxWVPImYFgst",
        name: botName
    });



    //function called when bot starts
    bot.on("start", function () {

        //écrire un message dans les logs au lancement du bot
        var message = "Hey there ! The slack bot is starting !"
        logger.info(message);

        //générer la liste des utilisateurs
        logger.info("Generate user list");
        slackHelper.generateUserList(bot);

        logger.info("Generate channel list")
        slackHelper.generateChannelList(bot);

    });

    //fonction qui sera appelée à chaque fois que le bot détectera un événement de type "message"
    bot.on("message", function (data) {

        try {
            if (data.type !== "message" || data.username == ("Haibot") || data.username == botName || data.user == "USLACKBOT") {
                return;
            }
            //displayFullNotification(data);
            var message = data.text;
            var channelId = data.channel;
            var channelName = slackHelper.getChannelnameFromId(channelId);
            var userId = data.user;
            var userName = slackHelper.getUsernameFromId(userId);

            logger.debug("Message received from user/id [" + userId + "/" + userName + "] on channel/id [" + channelId + "/" + channelName + "]. Message : [" + message + "]");



            if (message == "marvin build previouslyon") {
                handlePreviouslyOnCommand(data);
            }

            if (message == "marvin planning manage") {
                handlePlanningReminderCommand(true);
            }

            if (message !== undefined) {
                handleMessage(data, message.toLowerCase());
            }

        } catch (error) {
            logger.error("Error while receiving message  :" + error);
        }


    });

    bot.on("close", function (data) {
        logger.debug("Closing of the bot : " + displayFullNotification(data));
    });

    bot.on("error", function (data) {
        logger.debug("Error with the bot : " + displayFullNotification(data));
    });

    bot.on("open", function (data) {
        logger.debug("Opening of the bot : " + displayFullNotification(data));
    });



}

function postDevChannel(message) {
    bot.postMessageToChannel(devChannel, message, { username: botName });
}

function postToChannel(message, channel) {
    bot.postMessageToChannel(channel, message, { username: botName });
}

function handleMessage(data, message) {
    switch (message) {
        case "hi":
        case "hello":
            sendGreeting();
            break;
        case "marvin userlist":
            postDevChannel(slackHelper.userListToString());
            break;
        case "marvin channellist":
            logger.debug("action : marvin channellist");
            postDevChannel(slackHelper.channelListToString());
            break;
        case "marvin update spreadsheets":
            updateSpreadsheetsDatas();
            postDevChannel("Update spreadsheets OK");
            break;
        case "marvin planning reminder":
            handlePlanningReminderCommand(data);
            //postDevChannel("Update spreadsheets OK");
            break;
        case "marvin bills reminder":
            logger.debug("Action : marvin bills Reminder");
            handleBillsReminder();
            break;
        case botName + " margebrute":
            //bot.postMessageToUser("ilias", gss.displayMargeBrute());
            break;
        //si on ne reconnait pas le mot cle, alors on cherche si ça correspond a une question/reponse
        default:
            var keys = gssQA.getKeys();
            var values = gssQA.getValues();

            for (let index = 0; index < keys.length; index++) {
                var key = keys[index].trim();
                var splittedKeys = key.split('//');

                var keywordFound = false;

                for (let indexKey = 0; indexKey < splittedKeys.length; indexKey++) {
                    var word = splittedKeys[indexKey].trim();
                    if (message.includes(word.toLowerCase())) {
                        keywordFound = true;
                    }
                }

                if (keywordFound) {
                    var valuesForKeyword = values[index];
                    var randomIndex = Math.floor(Math.random() * Math.floor(valuesForKeyword.length));


                    var channelName = slackHelper.getChannelnameFromId(data.channel);

                    if (channelName == "Channel Not Found") {
                        bot.postMessageToUser(slackHelper.getUsernameFromId(data.user), valuesForKeyword[randomIndex], { username: botName });
                    } else if (channelName != "general") {
                        postToChannel(valuesForKeyword[randomIndex], channelName);
                    }
                }
            }
            return;
    }

}

function sendGreeting() {
    var greeting = getGreeting();
    postDevChannel(greeting);
}

function getGreeting() {
    var greetings = [
        "hello!",
        "hi there!",
        "cheerio!",
        "how do you do!",
        "¡hola!"
    ];
    return greetings[Math.floor(Math.random() * greetings.length)];
}

function handlePreviouslyOnCommand(data) {
    logger.info("Début handle previouslyon");

    var message = data.text;
    var userName = slackHelper.getUsernameFromId(data.user);

    var planning = gssPS.getPlanning();

    //calcul date du lundi précédent
    var options = { day: 'numeric', month: 'short' };
    var previousMonday = new Date();
    previousMonday.setDate(previousMonday.getDate() - (previousMonday.getDay() + 6) % 7);
    var previousMondayString = previousMonday.toLocaleDateString(languageLocale, options);
    logger.debug("previous monday string : " + previousMondayString);

    //recuper indice premiere colonne semaine
    var indexWeek = 0;
    var cptIndexWeek = 0;

    logger.debug("taille planning 1 : " + planning[1].length);

    while (indexWeek == 0 & indexWeek < 2000) {
        var dateCell = planning[1][cptIndexWeek];
        logger.debug("date cell : " + dateCell);
        if (dateCell.trim() == previousMondayString.trim()) {
            indexWeek = cptIndexWeek;
        } else {
            cptIndexWeek++;
        }

    }

    //récupérer index pour l'utilisateur
    var userIndex = 0;
    //lire la semaine pour le user donné
    for (let index = 0; index < planning.length; index++) {
        const row = planning[index];
        if (row[0] != undefined && (row[0].trim().toLowerCase() == userName.trim().toLowerCase())) {
            userIndex = index;
            //logger.debug("row of the user : " + index);
        }

    }

    postDevChannel(gssPS.displayWeekPreviouslyOn(indexWeek, userIndex));

}

function handlePlanningReminderCommand(notifyManager) {
    logger.info("Début handle planning reminder");

    var today = new Date();

    var planning = gssPS.getPlanning();

    var reminderResult = [];
    var userSlackname = "";
    var userUpdateString = "";
    var userUpdateParts = "";
    var userUpdateDate;
    var botUpdateString = "";
    var botUpdateParts = "";
    var botUpdateDate;

    for (let index = 0; index < planning.length; index++) {
        const row = planning[index];
        userSlackname = row[1];
        userUpdateString = row[2];
        botUpdateString = row[3];

        if (userSlackname !== undefined && userSlackname != "" && !userSlackname.startsWith("PO/PM") && !userSlackname.startsWith("DESIGNER") && userSlackname != "x") {
            if (userUpdateString !== undefined && userUpdateString != "") {
                userUpdateParts = userUpdateString.split("/");
                userUpdateDate = new Date(userUpdateParts[2], userUpdateParts[1] - 1, + userUpdateParts[0]);
            }

            if (botUpdateString !== undefined && botUpdateString != "") {
                botUpdateParts = botUpdateString.split("/");
                botUpdateDate = new Date(botUpdateParts[2], botUpdateParts[1] - 1, + botUpdateParts[0]);
            }

            if (userUpdateDate !== undefined && botUpdateDate !== undefined) {
                reminderResult.push([userSlackname, userUpdateDate, botUpdateDate, "D" + (index + 1)]);
            }
        }
    }

    postDevChannel("Full tab length : " + reminderResult.length);

    //These Data are NOT OK. A reminder should be sent to the user IF none has been sent in the last 7 days
    var reminderNOK = [];

    //These data are OK. Nothing to do
    var reminderOK = [];

    reminderResult.forEach(function (result) {
        var user = result[0];
        var userDate = result[1];
        var botDate = result[2];
        var userDateseven = new Date(userDate);
        var botDateseven = new Date(botDate);

        userDateseven.setDate(userDate.getDate() + 6);
        botDateseven.setDate(botDate.getDate() + 6);

        if ((userDateseven.getTime() < today.getTime())) {
            //send a notification to these users : their data are not OK and they haven't been reminded in the last 7 days
            reminderNOK.push(result);
        } else {
            //nothing to do here but thank them ;)
            reminderOK.push(result);
        }
    });

    postDevChannel("OK tab length : " + reminderOK.length);
    postDevChannel("NOK tab length : " + reminderNOK.length);

    logger.debug("Reminder users OK : ");
    reminderOK.forEach(element => {
        logger.debug("user : " + element[0] + " botDate cell : " + element[3]);
        //gssPS.updatePlanningCell(element[3], "TEST");
        //bot.postMessageToUser(element[0], "Les données de ton planning sont à jour. Merci à toi, c'est très pratique pour tes collègues :) !");
    });

    logger.debug("Reminder users NOK : ");
    reminderNOK.forEach(element => {
        logger.debug("user : " + element[0] + " botDate cell : " + element[3]);
        gssPS.updatePlanningCell(element[3], dateFormat(today, "dd/mm/yyyy"));
        
        if(notifyManager)
        {
            bot.postMessageToUser("felipe", "Le planning de staffing de la personne suivante n'est pas à jour : " + element[0]);
        } else
        {
            bot.postMessageToUser(element[0], "Ton staffing n'est pas à jour, peux-tu y jeter un coup d'oeil stp ? *(et n'oublie pas de mettre à jour la colonne C avec la date de mise à jour !)* : https://docs.google.com/spreadsheets/d/1TdW_GHZPEQQa5w4rCJTC7R4gNCs2dPudwVLPXgwnlb8/edit#gid=1405579711 \n ");
        }

    });

}

//update data from the spreadsheets. Should be called to be sure to see the very last data on the sheets
function updateSpreadsheetsDatas() {
    gssMB.getMargeBrute();
    gssQA.getQA();
    gssPS.fetchPlanning();
    gssBills.fetchBills();
}

//display all fields in a notification received by the bot
function displayFullNotification(data) {
    for (key in data) {
        if (data.hasOwnProperty(key)) {
            var value = data[key];
            logger.debug(key + ":" + value);
        }
    }
}

function handleBillsReminder() {
    var bills = gssBills.getBills();
    logger.debug("Number of bills : " + bills.length);
    bills.forEach(row => {
        var clientLead = row[4];
        var codeMission = row[5];
        var codeProjet = row[6]
        var numFacture = row[7];
        var montantHT = row[9];
        var dateFactureQB = row[12];
        var statusRetard = row[21];
        var clientLeadSlackId = row[row.length - 1];

        if (statusRetard == "retard") {
            var intro = "Hello " + clientLead + ". Il semblerait que la facture" + numFacture + "  pour le projet " + codeProjet + " soit en retard. Pourrais-tu y jeter un coup d'oeil stp ? Merci ! ";
            var reminder = "Code Mission : [" + codeMission + "]. Montant HT : [" + montantHT + "]. Date Facture Quickbook : [" + dateFactureQB + "]\n";

            bot.postMessageToUser(clientLeadSlackId, intro + "\n "  + reminder);
            bot.postMessageToUser("alexandre", "Bonjour Monsieur le DAF. Un message de rappel a été envoyé à " + clientLead + " pour la facture " + numFacture + " qui est indiquée comme étant en retard.");

        }

    });

}