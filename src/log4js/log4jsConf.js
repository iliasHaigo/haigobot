const log4js = require('log4js');

module.exports = {
    info : function(message) {
      info(message);
    },
    debug : function(message) {
      debug(message);
    },
    error : function(message) {
      error(message);
    },
    fatal : function(message) {
      fatal(message);
    },
    trace : function(message) {
      trace(message);
    }
  };

var logger;

function getLogger(){
    if(logger == null || logger === undefined){
        initLogger();
    }
    return logger;
}

function initLogger() {
    log4js.configure(
        {
          appenders: {
            file: {
              type: 'file',
              filename: 'logs/haigobot_debug.log',
              maxLogSize: 10 * 1024 * 1024, // = 10Mb
              backups: 5, // keep five backup files
              compress: true, // compress the backups
              encoding: 'utf-8',
              keepFileExt : true,
              layout: {
                type: 'pattern',
                pattern: "[%d{yyyy-MM-dd hh:mm:ss.SSS}] [%p] %c %m",
              }
            //   mode: 0o0640,
            //   flags: 'w+'
            },
            out: {
              type: 'stdout',
              layout: {
                type: 'pattern',
                pattern: '[%d{yyyy-MM-dd hh:mm:ss.SSS}] [%p] %c %m',
              }
            }
          },
          categories: {
            default: { appenders: ['file', 'out'], level: 'trace' }
          }
        }
      );
      
      logger = log4js.getLogger('debug');
    //   logger.debug('This little thing went to market');
      logger.info('Initialization of the logger OK');
    //   logger.error('This little thing had roast beef');
    //   logger.fatal('This little thing had none');
    //   logger.trace('and this little thing went wee, wee, wee, all the way home.');
}

function info(message){
  getLogger().info(message);
}

function debug(message){
  getLogger().debug(message);
}

function error(message){
  getLogger().error(message);
}

function fatal(message){
  getLogger().fatal(message);
}

function trace(message){
  getLogger().trace(message);
}
