const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const logger = require("../log4js/log4jsConf");

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

var planning;


module.exports = {
  fetchPlanning: function (callbackPlanning) {
    // Load client secrets from a local file.
    fs.readFile('credentials.json', (err, content) => {
      if (err) return logger.error('Error loading client secret file:' + err);
      // Authorize a client with credentials, then call the Google Sheets API.
      try {
        authorize(JSON.parse(content), fetchPlanning, callbackPlanning);
      }
      catch (error) {
        logger.error("Error while parsing content :" + error);
      }


    })
  },
  getPlanning: function () {
    return planning;
  },
  displayWeekPreviouslyOn: function (indexDay, indexUser) {
    return displayWeekPreviouslyOn(indexDay, indexUser);
  },
  updatePlanningCell: function (cell, value) {
    // Load client secrets from a local file.
    fs.readFile('credentials.json', (err, content) => {
      if (err) return logger.error('Error loading client secret file:' + err);
      try {
        // Authorize a client with credentials, then call the Google Sheets API.
        authorize(JSON.parse(content), updatePlanningCell.bind({ "cell": cell, "value": value }));
      }
      catch (error) {
        logger.error("Error while parsing content :" + error);

      }


    })
  }
};


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback, callbackPlanning) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    try {
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client, callbackPlanning);

    } catch (error) {
      logger.error("Error while parsing token :" + error);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback, callbackPlanning) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  logger.debug('Authorize this app by visiting this url:' + authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return logger.error('Error while trying to retrieve access token' + err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) logger.error(err);
        logger.debug('Token stored to' + TOKEN_PATH);
      });
      callback(oAuth2Client, callbackPlanning);
    });
  });
}

/**
 * fichier planning staffing : https://docs.google.com/spreadsheets/d/1TdW_GHZPEQQa5w4rCJTC7R4gNCs2dPudwVLPXgwnlb8/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function fetchPlanning(auth, callbackPlanning) {
  const sheets = google.sheets({ version: 'v4', auth });
  sheets.spreadsheets.values.get({
    spreadsheetId: '1TdW_GHZPEQQa5w4rCJTC7R4gNCs2dPudwVLPXgwnlb8',
    range: '2019!A1:TI112',
  }, (err, res) => {
    if (err) return logger.debug('The API returned an error: ' + err);
    const rows = res.data.values;
    if (rows.length) {

      //logger.debug("rows length : " + rows.length);
      //logger.debug("name A5 : " + rows[4][0]);
      planning = rows;
      logger.info("Planning Data Fetched");
      if (callbackPlanning != undefined) {
        callbackPlanning(planning);
      }
    } else {
      logger.info('No data found.');
    }
  });

}

function updatePlanningCell(auth, cell, value) {
  const sheets = google.sheets({ version: 'v4', auth });
  let values = [
    [
      this.value
    ],
    // Additional rows ...
  ];
  const resource = {
    values,
  };
  sheets.spreadsheets.values.update({
    spreadsheetId: '1TdW_GHZPEQQa5w4rCJTC7R4gNCs2dPudwVLPXgwnlb8',
    range: '2019!' + this.cell,
    valueInputOption: 'RAW',
    resource,
  }, (err, result) => {
    if (err) {
      // Handle error
      logger.error(err);
    } else {
      logger.debug('Cells updated.' + result.updatedCells);
    }
  });
}

//cette fonction va construire une chaîne de caractère de previouslyOn à partir de la 1ère case du lundi de la semaine dans le fichier de planning (mondayIndex = colonne, userIndex = ligne)
function displayWeekPreviouslyOn(mondayIndex, userIndex) {
  var result = "Hello ! Voici le planning de " + planning[userIndex][0] + " pour la semaine du " + planning[1][mondayIndex] + "\n";
  //LUNDI
  result += "*LUNDI*\n";
  result += ">" + planning[userIndex][mondayIndex] + "\n";
  result += ">" + planning[userIndex][mondayIndex + 1] + "\n";
  //MARDI
  result += "*MARDI*\n";
  result += ">" + planning[userIndex][mondayIndex + 2] + "\n";
  result += ">" + planning[userIndex][mondayIndex + 3] + "\n";
  //MERCREDI
  result += "*MERCREDI*\n";
  result += ">" + planning[userIndex][mondayIndex + 4] + "\n";
  result += ">" + planning[userIndex][mondayIndex + 5] + "\n";
  //JEUDI
  result += "*JEUDI*\n";
  result += ">" + planning[userIndex][mondayIndex + 6] + "\n";
  result += ">" + planning[userIndex][mondayIndex + 7] + "\n";
  //VENDREDI
  result += "*VENDREDI*\n";
  result += ">" + planning[userIndex][mondayIndex + 8] + "\n";
  result += ">" + planning[userIndex][mondayIndex + 9] + "\n";

  return result;

}

