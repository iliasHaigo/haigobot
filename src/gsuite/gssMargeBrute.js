const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const logger = require("../log4js/log4jsConf");

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

var margeBrute = "Client, Code Mission, Marge brute totale 2019: \n";

module.exports = {
  getMargeBrute: function () {
    // Load client secrets from a local file.
    fs.readFile('credentials.json', (err, content) => {
      if (err) return logger.error('Error loading client secret file:' + err);
      try {
        // Authorize a client with credentials, then call the Google Sheets API.
        authorize(JSON.parse(content), listMajors);
      } catch (error) {
        logger.error("Error while parsing content :" + error);
      }

    })
  },
  displayMargeBrute: function () {
    return margeBrute;
  }
};

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    try {
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    } catch (error) {
      logger.error("Error while parsing content :" + error);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  logger.debug('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return logger.error('Error while trying to retrieve access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) logger.error(err);
        logger.debug('Token stored to' + TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * fichier marge brute : https://docs.google.com/spreadsheets/d/1LW3JLKZaGKKgG62ZRtNv1M_Zq1VjnDJsoG0QBgKU4Bo/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function listMajors(auth) {
  const sheets = google.sheets({ version: 'v4', auth });
  sheets.spreadsheets.values.get({
    spreadsheetId: '1LW3JLKZaGKKgG62ZRtNv1M_Zq1VjnDJsoG0QBgKU4Bo',
    range: '3a-SuiviMB 2019!A1:AV30',
  }, (err, res) => {
    if (err) return logger.error('The API returned an error: ' + err);
    const rows = res.data.values;
    if (rows.length) {
      // Print columns A and E, which correspond to indices 0 and 4.
      var cptRow = 0;
      var rowStartProjects = 10
      rows.forEach(row => {
        cptRow++;
        if (row[0] != "" && cptRow >= rowStartProjects) {
          margeBrute = margeBrute + `${row[0]}, ${row[1]}, ${row[47]}` + "\n";

        }
        //logger.debug(margeBrute);
      });
    } else {
      logger.debug('No data found.');
    }
  });
}