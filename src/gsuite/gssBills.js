const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const logger = require("../log4js/log4jsConf");
var slackbotHelper = require("../../src/slack/slackbotHelper");

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

var bills;


module.exports = {
  fetchBills: function (callbackBills) {
    // Load client secrets from a local file.
    fs.readFile('credentials.json', (err, content) => {
      if (err) return logger.error('Error loading client secret file:' + err);
      // Authorize a client with credentials, then call the Google Sheets API.
      try {
        authorize(JSON.parse(content), fetchBills, callbackBills);
      }
      catch (error) {
        logger.error("Error while parsing content :" + error);
      }


    })
  },
  getBills: function () {
    return bills;
  },
  doBillsReminder: function () {
    doBillsReminder();
  }
};


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback, callbackBills) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    try {
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client, callbackBills);

    } catch (error) {
      logger.error("Error while parsing token :" + error);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback, callbackBills) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  logger.debug('Authorize this app by visiting this url:' + authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return logger.error('Error while trying to retrieve access token' + err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) logger.error(err);
        logger.debug('Token stored to' + TOKEN_PATH);
      });
      callback(oAuth2Client, callbackBills);
    });
  });
}

/**
 * fichier factures staffing : https://docs.google.com/spreadsheets/d/1LW3JLKZaGKKgG62ZRtNv1M_Zq1VjnDJsoG0QBgKU4Bo/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function fetchBills(auth, callbackBills) {
  const sheets = google.sheets({ version: 'v4', auth });
  sheets.spreadsheets.values.get({
    spreadsheetId: '1LW3JLKZaGKKgG62ZRtNv1M_Zq1VjnDJsoG0QBgKU4Bo',
    range: '2-FacturesClients 2019!A6:AP200',
  }, (err, res) => {
    if (err) return logger.debug('The API returned an error: ' + err);
    const rows = res.data.values;
    if (rows.length) {
      bills = rows;
      logger.info("Bills Data Fetched");
      if (callbackBills != undefined) {
        callbackBills(bills);
      }
    } else {
      logger.info('No data found.');
    }
  });

}

function doBillsReminder() {
  logger.debug("Number of bills : " + bills.length);
  bills.forEach(row => {
    var clientLead = row[4];
    var codeMission = row[5];
    var codeProjet = row[6]
    var numFacture = row[7];
    var montantHT = row[9];
    var dateFactureQB = row[12];
    var statusRetard = row[21];
    var clientLeadSlackId = row[row.length - 1];

    if (statusRetard == "retard") {
      var intro = "Hello " + clientLead + ". Il semblerait que la facture" + numFacture + "  pour le projet " + codeProjet + " soit en retard. Pourrais-tu y jeter un coup d'oeil stp ? Merci ! ";
      var reminder = "Code Mission : [" + codeMission + "]. Montant HT : [" + montantHT + "]. Date Facture Quickbook : [" + dateFactureQB + "]\n";

      logger.debug("Message sent : " + intro + "\n" + reminder);

    }

  });

}
