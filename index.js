//requirements
var SlackBot = require("slackbots");
var CronJob = require('cron').CronJob;
var dateFormat = require('dateformat');

var slackbotHelper = require("./src/slack/slackbotHelper");
var gssMB = require("./src/gsuite/gssMargeBrute");
var gssQA = require("./src/gsuite/gssQuestionsReponses");
var gssPS = require("./src/gsuite/gssPlanningStaffing");
var gssBills = require("./src/gsuite/gssBills");

//webserver
const ws = require("./webserver");
const logger = require("./src/log4js/log4jsConf");

//global variables
var userList = [];

logger.info("APPLICATION START. INITIALIZATION")

//initialisation du bot
slackbotHelper.initSlackbot();

//init web server
ws.initServer();

//init spreadsheets data
updateSpreadsheetsDatas();

function updateSpreadsheetsDatas() {
  logger.info("Updating Spreadsheets Data");
  gssMB.getMargeBrute();
  gssQA.getQA();
  gssPS.fetchPlanning();
  gssBills.fetchBills();
}


var cronUpdateData = new CronJob('0 0 9 * * *', function () {
  updateSpreadsheetsDatas();
}, null, true, 'Europe/Paris');

var cronStayingAlive = new CronJob('0 */10 * * * *', function () {
  //slackbotHelper.postDevChannel("Tic Tac, je suis en marche et il est " + dateFormat(new Date(), "h:MM"));
  logger.debug("trying to reconnect the bot of the bot");
  slackbotHelper.botConnect();
  updateSpreadsheetsDatas();
  //slackbotHelper.initSlackbot();
}, null, true, 'Europe/Paris');

var cronBillsReminder = new CronJob('0 0 11 * * 1', function () {
  slackbotHelper.billsReminder();
}, null, true, 'Europe/Paris');

var cronPlanningReminder = new CronJob('0 0 12 * * 1', function () {
  slackbotHelper.planningReminder(false);
}, null, true, 'Europe/Paris');

//notify felipe on tuesday 5pm
var cronPlanningReminder = new CronJob('0 0 17 * * 1', function () {
  slackbotHelper.planningReminder(true);
}, null, true, 'Europe/Paris');


